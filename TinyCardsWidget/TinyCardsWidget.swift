import WidgetKit
import SwiftUI

struct SimpleEntry: TimelineEntry {
    let date: Date
    let value: String
    let image: String
}

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), value: "Nederlands", image: "dutch_cover")
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), value: "Nederlands", image: "dutch_cover")
        
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []
        
        let date = Date()
        var index = 0

        decks.forEach { deck in
            if deck.assetStatus == AssetType.fromBundle.rawValue {
                deck.cards.forEach { card in
                    let newDate = Calendar.current.date(byAdding: .second, value: index * 5, to: date)!

                    let entry = SimpleEntry(date: newDate, value: card.value, image: card.image+"_wid")

                    entries.append(entry)

                    index += 1
                }
            }
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        
        completion(timeline)
    }
}

struct TinyCardsWidgetEntryView : View {
    internal var entry: Provider.Entry
    private let paddingValue = CGFloat(16)

    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.orange, .red]), startPoint: .topLeading, endPoint: .bottomTrailing)
            
            VStack {
                Spacer()
                
                GeometryReader { geo in
                    HStack {
                        Spacer()
                                                
                        Image(entry.image)
                            .resizable()
                            .frame(width: geo.size.width / 2, height: geo.size.height / 2)
                            .cornerRadius(12)
                            .padding([.top], paddingValue)
                        
                        Spacer()
                    }
                    
                    HStack {
                        Spacer()
                        
                        Text(entry.value)
                            .font(Font.largeTitle.weight(.bold))
                            .foregroundColor(.white)
                            .frame(width: geo.size.width-paddingValue*2)
                            .minimumScaleFactor(0.5)
                            .lineLimit(1)
                            .multilineTextAlignment(.center)
                            .padding([.top], geo.size.height / 1.7)
                        
                        Spacer()
                    }
                }
            }
        }
    }
}

@main
struct TinyCardsWidget: Widget {
    let kind: String = "TinyCardsWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            TinyCardsWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("Tiny Cards Widget")
        .description("Learn Dutch!")
        .supportedFamilies([.systemSmall])
    }
}

struct TinyCardsWidget_Previews: PreviewProvider {
    static var previews: some View {
        TinyCardsWidgetEntryView(entry: SimpleEntry(date: Date(), value: "Nederlands", image: "dutch_cover"))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
