import AVFoundation

internal final class SoundPlayer {
    private var audioPlayer: AVAudioPlayer?
    
    internal func playSound(sound: String, type: String) {
        if let path = Bundle.main.path(forResource: sound, ofType: type) {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                audioPlayer?.play()
            } catch { }
        }
    }
}
