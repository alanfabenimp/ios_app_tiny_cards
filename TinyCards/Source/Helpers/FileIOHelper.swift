import Foundation

internal final class FileIOHelper {    
    @discardableResult
    internal func saveImageToFile(imageData: Data, fileName: String) -> Bool {
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        
        do {
            guard let path = directory.appendingPathComponent(fileName) else {
                return false
            }
            
            try imageData.write(to: path)
            
            return true
        } catch {
            return false
        }
    }
    
    @discardableResult
    internal func moveDeckFileToDocumentsDirectory() -> Bool {
        guard let file = Bundle.main.url(forResource: "decks.json", withExtension: nil) else {
            return false
        }
        
        guard let destination = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("decks.json") else {
            return false
        }
        
        do {
            try FileManager.default.copyItem(at: file, to: destination)
            
            return true
        } catch {
            return false
        }
    }
    
    @discardableResult
    internal func writeJSON(items: [Deck]) -> Bool {
        do {
            let fileURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent("decks.json")

            let encoder = JSONEncoder()
            try encoder.encode(items).write(to: fileURL)
            
            return true
        } catch {
            return false
        }
    }
    
    internal func loadImageFromFile(fileName: String) -> Data? {
        let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileUrl = directory.appendingPathComponent(fileName)
        
        let imageData = try? Data(contentsOf: fileUrl)
        
        return imageData
    }
}
