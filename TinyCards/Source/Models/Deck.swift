import Foundation
import UIKit

internal enum AssetType: Int {
    case fromBundle = 0, fromFileDirectory = 1
}

internal struct Deck: Hashable, Codable, Identifiable {
    internal var id: Int
    internal var name: String
    internal var description: String
    internal var difficulty: Int
    internal var image: String
    internal var assetStatus: Int        
    
    internal init(id: Int, name: String, description: String, difficulty: Int, image: String, assetStatus: AssetType, cards: [Card]) {
        self.id = id
        self.name = name
        self.description = description
        self.difficulty = difficulty
        self.image = image
        self.assetStatus = assetStatus.rawValue
        self.cards = cards
    }
    
    internal var difficultyDescription: String {
        switch difficulty {
        case 0:
            return "Easy level"
        case 1:
            return "Medium level"
        case 2:
            return "Hard level"
        default:
            return ""
        }
    }
    
    internal var cards: [Card]
    
    internal struct Card: Codable, Hashable {
        internal var id: Int
        internal var value: String
        internal var correctOption: Int
        internal var image: String
        internal var assetStatus: Int
        internal var options: [Option]
        
        internal init(id: Int, value: String, image: String, correctOption: Int, assetStatus: AssetType, options: [Deck.Card.Option]) {
            self.id = id
            self.image = image
            self.value = value
            self.correctOption = correctOption
            self.assetStatus = assetStatus.rawValue
            self.options = options
        }
        
        internal var correctOptionValue: String {
            for option in options {
                if option.id == correctOption {
                    return option.value
                }
            }
            
            return ""
        }
        
        internal struct Option: Codable, Hashable {
            internal var id: Int
            internal var value: String
            
            internal init(id: Int, value: String) {
                self.id = id
                self.value = value
            }
        }
    }
}
