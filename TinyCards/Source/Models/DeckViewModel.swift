import Foundation
import Combine
import UIKit

private enum DeckInformationStatus {
    case invalidName
    case invalidDescription
    case invalidImage
    case valid
}

internal final class DeckViewModel: ObservableObject {
    @Published var name: String = ""
    @Published var description: String = ""
    @Published var image: UIImage? = nil
    @Published var isValid = false
    @Published var deckInformationMessage: String = ""
    
    private var cancellableSet: Set<AnyCancellable> = []
    private let fileIOHelper = FileIOHelper()
    
    internal init() {
        isDeckInformationValidPublisher
            .receive(on: RunLoop.main)
            .map { status in
                switch status {
                case .invalidName:
                    self.isValid = false
                    
                    return "The deck name is invalid."
                case .invalidDescription:
                    self.isValid = false
                    
                    return "The deck description is invalid."
                case .invalidImage:
                    self.isValid = false
                    
                    return "The deck image is invalid."
                default:
                    self.isValid = true
                    
                    return ""
                }
            }
            .assign(to: \.deckInformationMessage, on: self)
            .store(in: &cancellableSet)
    }
    
    private var isNameValidPublisher: AnyPublisher<Bool, Never> {
        $name
            .debounce(for: 0.5, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { input in
                return input.count > 0
            }
            .eraseToAnyPublisher()
    }
    
    private var isDescriptionValidPublisher: AnyPublisher<Bool, Never> {
        $description
            .debounce(for: 0.5, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { input in
                return input.count > 0
            }
            .eraseToAnyPublisher()
    }
    
    private var isImageValidPublisher: AnyPublisher<Bool, Never> {
        $image
            .removeDuplicates()
            .map { input in
                return input != nil
            }
            .eraseToAnyPublisher()
    }
    
    private var isDeckInformationValidPublisher: AnyPublisher<DeckInformationStatus, Never> {
        Publishers.CombineLatest3(isNameValidPublisher, isDescriptionValidPublisher, isImageValidPublisher)
            .map { nameIsValid, descriptionIsValid, imageIsValid in
                if nameIsValid == false {
                    return .invalidName
                }

                if descriptionIsValid == false {
                    return .invalidDescription
                }
                
                if imageIsValid == false {
                    return .invalidImage
                }
                
                return .valid
            }
            .eraseToAnyPublisher()
    }
    
    internal func createNewDeck() -> Deck? {
        if let imageData = image?.pngData() {
            let imageFileName = UUID().uuidString.lowercased()+".png"
            let deckId = Int.random(in: 0..<9999)
            
            fileIOHelper.saveImageToFile(imageData: imageData, fileName: imageFileName)
            
            let newDeck = Deck(id: deckId,
                               name: name,
                               description: description,
                               difficulty: 0,
                               image: imageFileName,
                               assetStatus: AssetType.fromFileDirectory,
                               cards: [])
            
            name = ""
            description = ""
            image = nil
            
            return newDeck
        }
        
        return nil
    }
}
