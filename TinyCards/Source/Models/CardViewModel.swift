import Foundation
import Combine
import UIKit

private enum CardInformationStatus {
    case invalidValue
    case invalidImage
    case valid
}

internal final class CardViewModel: ObservableObject {
    @Published var value: String = ""
    @Published var image: UIImage? = nil
    @Published var isValid = false
    @Published var cardInformationMessage: String = ""
    
    private var cancellableSet: Set<AnyCancellable> = []
    private let fileIOHelper = FileIOHelper()
    
    internal init() {
        isCardInformationValidPublisher
            .receive(on: RunLoop.main)
            .map { status in
                switch status {
                case .invalidValue:
                    self.isValid = false
                    
                    return "The card value is invalid."
                case .invalidImage:
                    self.isValid = false
                    
                    return "The card image is invalid."
                default:
                    self.isValid = true
                    
                    return ""
                }
            }
            .assign(to: \.cardInformationMessage, on: self)
            .store(in: &cancellableSet)
    }
    
    private var isValueValidPublisher: AnyPublisher<Bool, Never> {
        $value
            .debounce(for: 0.5, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { input in
                return input.count > 0
            }
            .eraseToAnyPublisher()
    }
    
    private var isImageValidPublisher: AnyPublisher<Bool, Never> {
        $image
            .removeDuplicates()
            .map { input in
                return input != nil
            }
            .eraseToAnyPublisher()
    }
    
    private var isCardInformationValidPublisher: AnyPublisher<CardInformationStatus, Never> {
        Publishers.CombineLatest(isValueValidPublisher, isImageValidPublisher)
            .map { nameIsValid, imageIsValid in
                if nameIsValid == false {
                    return .invalidValue
                }
                
                if imageIsValid == false {
                    return .invalidImage
                }
                
                return .valid
            }
            .eraseToAnyPublisher()
    }
    
    internal func createNewFlashcard() -> Deck.Card? {
        if let imageData = image?.pngData() {
            let imageFileName = UUID().uuidString.lowercased()+".png"
            
            fileIOHelper.saveImageToFile(imageData: imageData, fileName: imageFileName)
            
            let cardId = Int.random(in: 0..<9999)
            let valueForCorrectOption = Int.random(in: 0..<2)
                                    
            var availableOptions = [1, 2, 3]
            var options: [Deck.Card.Option] = []
            
            let option1 = Deck.Card.Option(id: valueForCorrectOption, value: value)
            let option2 = Deck.Card.Option(id: availableOptions.popLast()!, value: words.randomElement() ?? "")
            let option3 = Deck.Card.Option(id: availableOptions.popLast()!, value: words.randomElement() ?? "")
            
            options.append(option1)
            options.append(option2)
            options.append(option3)
            
            options.shuffle()
            
            let newCard = Deck.Card(id: cardId,
                                    value: value.lowercased(),
                                    image: imageFileName,
                                    correctOption: valueForCorrectOption,
                                    assetStatus: AssetType.fromFileDirectory,
                                    options: options)
            
            value = ""
            image = nil

            return newCard
        }
        
        return nil
    }
}
