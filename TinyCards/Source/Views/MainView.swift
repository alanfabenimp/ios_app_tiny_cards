import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            DeckList()
                .tabItem {
                    Label("My Decks", systemImage: "square.grid.2x2")
                }
            NewDeck()
                .tabItem {
                    Label("New Deck", systemImage: "plus.bubble")
                }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
