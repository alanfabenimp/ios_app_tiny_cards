import SwiftUI

struct CongratulationsView: View {
    @State private var isMainViewPresented = false
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                
                Text("Congratulations on your achievement!")
                    .foregroundColor(.orange)
                    .font(Font.title.bold())
                    .multilineTextAlignment(.center)
                
                Spacer()
            }
            
            Image("reward")
                .resizable()
                .frame(width: 300, height: 300)
            
            LargeButton(action: {
                isMainViewPresented.toggle()
            }, value: "Keep learning!")
        }
        .navigationBarHidden(true)
        .fullScreenCover(isPresented: $isMainViewPresented, content: MainView.init)
    }
}

struct CongratulationsView_Previews: PreviewProvider {
    static var previews: some View {
        CongratulationsView()
    }
}
