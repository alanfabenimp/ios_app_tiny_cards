import SwiftUI

struct DeckRow: View {
    private let fileIOHelper = FileIOHelper()
    
    internal var deck: Deck
    
    private func deckImage() -> UIImage {
        let defaultImage = UIImage(named: "dutch_cover")!
        
        if deck.assetStatus == 0 {
            return UIImage(named: deck.image) ?? defaultImage
        } else {
            guard let imageData = fileIOHelper.loadImageFromFile(fileName: deck.image) else {
                return defaultImage
            }
            
            return UIImage(data: imageData) ?? defaultImage
        }
    }
    
    var body: some View {
        HStack(alignment: .top) {
            Image(uiImage: deckImage())
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 125, height: 125)
            
            VStack(alignment: .leading) {
                Text(deck.name)
                    .font(Font.title.weight(.bold))
                    .foregroundColor(Color("title"))
                    .padding(.bottom, 2)
                
                Text(deck.description)
                    .font(Font.callout.weight(.semibold))
                    .foregroundColor(.secondary)
                
                Spacer()
                
                Text(deck.difficultyDescription)
                    .font(Font.footnote.weight(.semibold))
                    .foregroundColor(.secondary)
            }.frame(height: 125)
            
            Spacer()
        }
    }
}

struct DeckRow_Previews: PreviewProvider {
    static var previews: some View {
        DeckRow(deck: decks[0])
    }
}
