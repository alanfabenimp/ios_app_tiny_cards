import SwiftUI

struct DeckList: View {
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVStack(spacing: 16, content: {
                    ForEach(decks, id: \.id) { deck in
                        NavigationLink(destination: FlashcardView(cards: deck.cards)) {
                            DeckRow(deck: deck)
                        }
                    }
                })
                .padding()
                .accessibility(addTraits: .isButton)
                .accessibility(identifier: "deckRow")
            }.navigationBarHidden(true)
        }
    }
}

struct DeckList_Previews: PreviewProvider {
    static var previews: some View {
        DeckList()
    }
}
