import SwiftUI

internal enum ActiveFullscreenView: Identifiable {
    case photoGallery, newFlashCard(deck: Deck)

    internal var id: Int {
        switch self {
        case .photoGallery:
            return 0
        case .newFlashCard(_):
            return 1
        }
    }
}

struct NewDeck: View {
    @State private var isShowingPhotoLibrary = false
    @State private var activeFullscreenView: ActiveFullscreenView?
        
    @ObservedObject private var deckViewModel = DeckViewModel()
    
    var body: some View {
        VStack {
            Section {
                ImagePickerContainer(image: $deckViewModel.image).onTapGesture {
                    activeFullscreenView = .photoGallery
                }
            }
            
            Section {
                Form {
                    TextField("Name", text: $deckViewModel.name)
                    TextField("Description", text: $deckViewModel.description)
                }
            }
            
            Section {
                LargeButton(action: {
                    if let newDeck = deckViewModel.createNewDeck() {
                        activeFullscreenView = .newFlashCard(deck: newDeck)
                    }
                }, value: "Save this deck").disabled(deckViewModel.isValid == false)
            }
            
            Section(footer: Text(deckViewModel.deckInformationMessage).foregroundColor(.red)) {}
        }
        .padding(.top, 32)
        .padding(.bottom, 32)        
        .fullScreenCover(item: $activeFullscreenView) { item in
            switch item {
                case .photoGallery:
                    ImagePicker(sourceType: .photoLibrary, selectedImage: $deckViewModel.image)
                case .newFlashCard(let deck):
                    NewFlashcard(deck: deck)
            }
        }
    }
}

struct NewDeck_Previews: PreviewProvider {
    static var previews: some View {
        NewDeck()
    }
}
