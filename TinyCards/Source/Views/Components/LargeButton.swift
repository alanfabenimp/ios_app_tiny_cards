import SwiftUI

struct LargeButton: View {
    internal let action: () -> Void
    internal var value: String
    
    var body: some View {
        Button(action: action) {
            Text(value)
        }
        .frame(width: 300, height: 20)
        .font(Font.headline.weight(.bold))
        .foregroundColor(.accentColor)
        .padding()
        .overlay(
            RoundedRectangle(cornerRadius: 12)
                .stroke(lineWidth: 2)
                .foregroundColor(.accentColor)
        )
    }
}

struct LargeButton_Previews: PreviewProvider {
    static var previews: some View {
        LargeButton(action: {
            print("nothing happens")
        }, value: "touch my button")
    }
}


