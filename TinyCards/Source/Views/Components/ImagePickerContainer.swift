import SwiftUI

struct ImagePickerContainer: View {
    @State private var isShowingPhotoLibrary = false
    @Binding internal var image: UIImage?
    
    var body: some View {
        ZStack {
            Rectangle()
                .frame(width: 200, height: 200)
                .foregroundColor(.clear)
                .border(Color.orange, width: 4)
                .cornerRadius(12)
            
            Image(systemName: "plus.bubble")
            
            if let image = image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 185, height: 185)
            }
        }
    }
}

struct ImagePickerContainer_Previews: PreviewProvider {
    static var previews: some View {
        Text("hey")
        //ImagePickerContainer(image: $image)
    }
}
