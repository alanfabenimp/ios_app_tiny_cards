import SwiftUI

internal enum ActiveNewFlashcardFullscreenView: Identifiable {
    case photoGallery, mainView

    internal var id: Int {
        hashValue
    }
}

struct NewFlashcard: View {
    @State private var newCards: [Deck.Card] = []
    @State private var activeNewFlashcardFullscreenView: ActiveNewFlashcardFullscreenView?
    
    @State internal var deck: Deck
    
    @ObservedObject private var cardViewModel = CardViewModel()
    
    private let fileIOHelper = FileIOHelper()
    
    var body: some View {
        VStack {
            Section {
                ImagePickerContainer(image: $cardViewModel.image).onTapGesture {
                    activeNewFlashcardFullscreenView = .photoGallery
                }
            }
            
            Section {
                Form {
                    TextField("Value", text: $cardViewModel.value)
                }
            }
            
            Section {
                LargeButton(action: {
                    if let newFlashcard = cardViewModel.createNewFlashcard() {
                        newCards.append(newFlashcard)
                    }
                }, value: "Save this flash card")
            }
            
            Section {
                LargeButton(action: {
                    deck.cards = newCards
                    
                    decks.append(deck)
                    
                    fileIOHelper.writeJSON(items: decks)
                    
                    activeNewFlashcardFullscreenView = .mainView
                }, value: "Finish")
            }
            
            Section(footer: Text(cardViewModel.cardInformationMessage).foregroundColor(.red)) {}
        }
        .padding(.top, 32)
        .padding(.bottom, 32)
        .fullScreenCover(item: $activeNewFlashcardFullscreenView) { item in
            switch item {
            case .photoGallery:
                ImagePicker(sourceType: .photoLibrary, selectedImage: $cardViewModel.image)
            case .mainView:
                MainView.init()
            }
        }
    }
}

struct NewFlashcard_Previews: PreviewProvider {
    static var previews: some View {
        NewFlashcard(deck: decks[0])
    }
}
