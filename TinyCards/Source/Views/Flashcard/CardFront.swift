import SwiftUI

struct CardFront: View {
    internal var image: UIImage
    
    var body: some View {
        Image(uiImage: image)
            .resizable()
            .frame(width: 300, height: 300)
            .overlay(
                RoundedRectangle(cornerRadius: 12)
                    .stroke(lineWidth: 3)
                    .foregroundColor(.accentColor)
            )
    }
}

struct CardImage_Previews: PreviewProvider {
    static var previews: some View {
        CardFront(image: UIImage(named: "dutch_cover")!)
    }
}
