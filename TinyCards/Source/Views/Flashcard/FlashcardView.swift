import SwiftUI

struct FlashcardView: View {
    internal var cards: [Deck.Card]
    
    @State private var flipped = false
    @State private var currentIndex = 0
    @State private var isLinkActive = false
    @State private var offset: CGFloat = 0
    @State private var index = 0
    
    var body: some View {
        VStack {
            HStack {
                BackButton()

                Spacer()
            }
            .padding([.top], 16)
            
            Spacer()
            
            GeometryReader { geometry in
                ScrollView(.horizontal) {
                    HStack {
                        ForEach(cards, id: \.id) { card in
                            Flashcard(card: card)
                                .frame(width: geometry.size.width)
                        }
                    }
                    
                }                
                .content.offset(x: offset)
                .frame(width: geometry.size.width, alignment: .leading)
                .padding(.top, geometry.size.width / 2)
                .gesture(
                    DragGesture()
                        .onChanged({ value in
                            offset = value.translation.width - geometry.size.width * CGFloat(index)
                        })
                        .onEnded({ value in
                            if -value.predictedEndTranslation.width > geometry.size.width / 2, index < cards.count - 1 {
                                index += 1
                            }
                            
                            if value.predictedEndTranslation.width > geometry.size.width / 2, index > 0 {
                                index -= 1
                            }
                            
                            withAnimation { offset = -(geometry.size.width + 10) * CGFloat(index) }
                        })
                )
            }
            
            Spacer()
            
            NavigationLink(destination: CardView(cards: cards), isActive: $isLinkActive) {
                LargeButton(action: {
                    isLinkActive.toggle()
                }, value: "Train your knowledge!")
            }.padding()
        }.navigationBarHidden(true)
    }
}

struct FlashcardView_Previews: PreviewProvider {
    static var previews: some View {
        FlashcardView(cards: decks[0].cards)
    }
}
