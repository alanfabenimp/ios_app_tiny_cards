import SwiftUI

struct CardBack: View {
    internal var value: String
    
    var body: some View {
        ZStack {
            Rectangle()
                .cornerRadius(12)
                .overlay(
                    RoundedRectangle(cornerRadius: 12)
                        .stroke(lineWidth: 3)
                        .foregroundColor(.accentColor)
                        .background(Color.white)
                )
                .frame(width: 300, height: 300)
                                    
            Text(value)
                .font(Font.largeTitle.weight(.bold))
                .foregroundColor(.accentColor)
        }
    }
}

struct CardBack_Previews: PreviewProvider {
    static var previews: some View {
        CardBack(value: "value")
    }
}
