import SwiftUI

struct Flashcard: View {
    private let fileIOHelper = FileIOHelper()
    
    internal var card: Deck.Card
    
    @State private var flipped = false
    
    private func cardImage() -> UIImage {
        let defaultImage = UIImage(named: "dutch_cover")!
        
        if card.assetStatus == 0 {
            return UIImage(named: card.image) ?? defaultImage
        } else {
            guard let imageData = fileIOHelper.loadImageFromFile(fileName: card.image) else {
                return defaultImage
            }
            
            return UIImage(data: imageData) ?? defaultImage
        }
    }
    
    var body: some View {
        let flipDegrees = flipped ? 180.0 : 0
        
        ZStack {
            CardFront(image: cardImage())
                .rotation3DEffect(Angle(degrees: flipDegrees), axis: (x: 0, y: 10.0, z: 0.0))
                .opacity(flipped ? 0.0 : 1.0)
            
            CardBack(value: card.correctOptionValue)
                .rotation3DEffect(Angle(degrees: -180 + flipDegrees), axis: (x: 0, y: 10.0, z: 0.0))
                .opacity(flipped ? 1.0 : 0.0)
        }
        .animation(.easeInOut(duration: 0.5))
        .onTapGesture {
            flipped.toggle()
        }
    }
}

struct Flashcard_Previews: PreviewProvider {
    static var previews: some View {
        Flashcard(card: decks[0].cards[0])
    }
}
