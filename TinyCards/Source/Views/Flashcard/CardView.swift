import SwiftUI

struct CardView: View {
    private let fileIOHelper = FileIOHelper()
    
    internal var cards: [Deck.Card]
    
    @State private var currentIndex = 0
    @State private var deckProgress = 0.0
    @State private var cardBorderColor: Color = .accentColor
    @State private var cardBorderLineWidth = CGFloat(2.0)    
    @State private var showCongratulationsScreen = false
    
    private let soundPlayer = SoundPlayer()
    
    private func cardOption(option: Deck.Card.Option) -> LargeButton {
        let button = LargeButton(action: {
            if option.id == cards[currentIndex].correctOption {
                if currentIndex+1 < cards.count {
                    currentIndex += 1
                    
                    deckProgress = Double(currentIndex) / Double(cards.count)
                                        
                    soundPlayer.playSound(sound: "success_", type: "m4a")
                } else {
                    soundPlayer.playSound(sound: "finished_", type: "m4a")
                    
                    deckProgress = 1
                    
                    showCongratulationsScreen.toggle()
                }
            } else {
                animateCardWithWrongOption()
                
                soundPlayer.playSound(sound: "error_", type: "m4a")
            }
        }, value: option.value)
        
        return button
    }
    
    private func animateCardWithWrongOption() {
        withAnimation(.linear(duration: 0.5)) {
            cardBorderColor = .red
            cardBorderLineWidth = 5
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
            withAnimation(.linear(duration: 0.5)) {
                cardBorderColor = .accentColor
                cardBorderLineWidth = 2
            }
        }
    }
    
    private func cardImage() -> UIImage {
        let defaultImage = UIImage(named: "dutch_cover")!
        
        if cards[currentIndex].assetStatus == 0 {
            return UIImage(named: cards[currentIndex].image) ?? defaultImage
        } else {
            guard let imageData = fileIOHelper.loadImageFromFile(fileName: cards[currentIndex].image) else {
                return defaultImage
            }
            
            return UIImage(data: imageData) ?? defaultImage
        }
    }
    
    var body: some View {
        VStack(spacing: 32) {
            HStack(spacing: 0, content: {
                BackButton()
                                                
                ProgressView(value: deckProgress)
                    .padding()
                    .scaleEffect(y: 2)
                    .accentColor(.accentColor)

                Spacer()
            }).padding([.trailing], 30)
            
            CardFront(image: cardImage())
                .overlay(
                    RoundedRectangle(cornerRadius: 12)
                        .stroke(lineWidth: cardBorderLineWidth)
                        .foregroundColor(cardBorderColor)
                )
            
            ScrollView(content: {
                LazyVStack(spacing: 20, content: {
                    ForEach(cards[currentIndex].options, id: \.id) { option in
                        cardOption(option: option)
                    }
                }).padding()
            })
        }
        .navigationBarHidden(true)
        .sheet(isPresented: $showCongratulationsScreen) {
            CongratulationsView()
        }
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(cards: decks[0].cards)
    }
}
