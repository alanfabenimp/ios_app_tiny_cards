import Foundation
import UIKit

internal class AppDelegate: NSObject, UIApplicationDelegate {
    private let fileIOHelper = FileIOHelper()
    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        fileIOHelper.moveDeckFileToDocumentsDirectory()
        
        return true
    }
}
