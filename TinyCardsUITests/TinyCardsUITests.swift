import XCTest

class TinyCardsUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launchArguments = ["testing"]
        app.launch()
    }
    
    func testThatTabBarIsSetupCorrectly() throws {
        let tabBars = app.tabBars
        
        XCTAssertTrue(tabBars.buttons["My Decks"].exists)
        XCTAssertTrue(tabBars.buttons["New Deck"].exists)
    }
    
    func testThatNavigationBetweenDecksListAndFlashcardViewWorks() throws {
        app.buttons["deckRow"].tap()
                
        let trainYourKnowledgeButton = app.buttons["Train your knowledge!"]
        XCTAssertEqual(trainYourKnowledgeButton.waitForExistence(timeout: 10), true)
    }
    
    func testThatNavigationBetweenFlashcardViewAndCardViewWorks() throws {
        app.buttons["deckRow"].tap()
                
        let trainYourKnowledgeButton = app.buttons["Train your knowledge!"]
        XCTAssertEqual(trainYourKnowledgeButton.waitForExistence(timeout: 10), true)
               
        trainYourKnowledgeButton.tap()
        
        let progressView = app.progressIndicators.element
        XCTAssertEqual(progressView.waitForExistence(timeout: 10), true)
    }
    
    func testThatBackButtonInFlashcardViewWorks() throws {
        app.buttons["deckRow"].tap()
        
        let backButton = app.buttons["arrow.backward"]
        XCTAssertEqual(backButton.waitForExistence(timeout: 10), true)
        
        backButton.tap()
                
        XCTAssertEqual(app.buttons["deckRow"].waitForExistence(timeout: 10), true)
    }
    
    func testThatBackButtonInCardViewWorks() throws {
        app.buttons["deckRow"].tap()
                
        let trainYourKnowledgeButton = app.buttons["Train your knowledge!"]
               
        trainYourKnowledgeButton.tap()
        
        let backButton = app.buttons["arrow.backward"]
        XCTAssertEqual(backButton.waitForExistence(timeout: 10), true)
        
        backButton.tap()
        
        let trainYourKnowledgeButtonAgain = app.buttons["Train your knowledge!"]
        XCTAssertEqual(trainYourKnowledgeButtonAgain.waitForExistence(timeout: 10), true)
    }
}
